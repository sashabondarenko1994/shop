<?php

namespace Shop\models;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//echo "shop project";



require_once __DIR__ . '/vendor/autoload.php';

//$loader = new \Twig\Loader\FilesystemLoader('Shop/templates/');
//$twig = new \Twig\Environment($loader);
//echo $twig->render('index.html');
//die("1");


$klein = new \Klein\Klein();




//Main page
$klein->respond(['GET', 'POST'], '/', function () {

    $loader = new \Twig\Loader\FilesystemLoader('Shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('index.html');

    //return 'Hello World!';
});





//Customer
$klein->respond(array('GET', 'POST'), '/customer', function (){

    $loader = new \Twig\Loader\FilesystemLoader(('Shop/templates/'));
    $twig = new \Twig\Environment($loader);
    return $twig->render('customer.html');

});

//Order
$klein->respond(array('GET', 'POST'), '/order', function (){

    $loader = new \Twig\Loader\FilesystemLoader(('Shop/templates/'));
    $twig = new \Twig\Environment($loader);
    return $twig->render('order.html');

});

//Cart
$klein->respond(array('GET', 'POST'), '/cart', function (){

    $loader = new \Twig\Loader\FilesystemLoader(('Shop/templates/'));
    $item = new Item(2,new Product(13,'Cars','Mercedes','S', 10000), 10000);
    $cart = new Cart();
    $cart->add($item);
    $twig = new \Twig\Environment($loader);
    return $twig->render('cart.twig', array('cart' => $cart, 'total1' => $cart->get_total()));

});

//Product

$klein->respond(array('GET', 'POST'), '/product', function (){

    $loader = new \Twig\Loader\FilesystemLoader(('Shop/templates/'));
    $twig = new \Twig\Environment($loader);
    return $twig->render('product.twig', array(
        'products' => array(
            new Product(1, new Category('Foot'),'adidas','predator','1000'),
            new Product(2, new Category('Head'),'nike','tiempo','1100'),
            new Product(3, new Category('Men'),'puma','evo','600'),
            new Product(5,new Category('All'),'Vagabond','Shoes','2000')
        )
    ));

});

//Category
$klein->respond(['GET', 'POST'], '/category', function () {

    $loader = new \Twig\Loader\FilesystemLoader('Shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('category.html');

});

$klein->dispatch();


//echo $product->__toString(); PHP_EOL;

