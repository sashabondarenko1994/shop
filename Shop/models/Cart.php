<?php

namespace Shop\models;


class Cart
{

    protected $items = array();

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }


    public function add($item) {
        array_push($this->items, $item);
    }

    public function remove($item){
        array_splice($this->items, $item);
    }


    public function get_total() {
        return array_reduce($this->items, function($sum, $item) {
            return $sum + $item->getPrice() * $item->getQuantity();
        }, 0);
    }
}
