<?php

namespace Shop\models;

//use Shop\models\Category;

class Product extends Category
{
    protected $id;
    protected $category;
    protected $brand;
    protected $model;
    protected $price;

    /**
     * Product constructor.
     * @param $id
     * @param $category
     * @param $brand
     * @param $model
     * @param $price
     */
    public function __construct($id, $category, $brand, $model, $price)
    {
        $this->id = $id;
        $this->category = $category;
        $this->brand = $brand;
        $this->model = $model;
        $this->price = $price;

    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


    public function __toString()
    {
        return $this->getModel() . ' ' . $this->getBrand() . ' '.  $this->getPrice();
    }
}